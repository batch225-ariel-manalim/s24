const getCube = Math.pow(5, 3);
console.log(`The cube of 5 is ${getCube}`);


const address = ["258 Washington Ave NW", "California 90011"]
const [country, state] = address
console.log(`I live at ${country}, ${state}`)

const animal = {
	weighed: 5,
	measurementFt: 2,
	measurementIn: 4
}

const {weighed, measurementFt, measurementIn} = animal


console.log(`Mia is a dog. He weighed at ${weighed} kgs with a measurement of ${measurementFt} ft ${measurementIn} in.`)



const numbers = [1,2,3,4,5];

numbers.forEach(number => {
	console.log(number);
})
const reduceNumber = numbers.reduce((total, number) => {
	return total + number
})
console.log(reduceNumber)


class Dog {
	constructor(name, age, breed){
		this.name = name;
		this.age = age;
		this.breed = breed;
	}
}
const dogDetails = new Dog('Mia', 3, 'Askal');
console.log(dogDetails)